package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/jessevdk/go-flags"
	"github.com/pkg/errors"
)

type config struct {
	Version  string `json:"version"`
	Instance string `json:"instance"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func readConfigFile(path string) (result config, err error) {
	buffer, err := ioutil.ReadFile(path)
	if err != nil {
		return result, errors.Wrap(err, "couldn't read config file")
	}

	err = json.Unmarshal(buffer, &result)
	if err != nil {
		return result, errors.Wrap(err, "couldn't parse config file")
	}

	return result, nil
}

type options struct {
	Config   string   `short:"c" long:"config" default:"./instance.json" description:"path of the config file"`
	ExecArgs execOpts `group:"Execute script Options"`
	Verbose  bool     `short:"v" long:"verbose" description:"be more verbose"`
	Args     struct {
		Script string `positional-arg-name:"FILE" required:"yes"`
	} `positional-args:"yes"`
}

func parseOptions() (result options) {
	_, err := flags.Parse(&result)
	if err != nil {
		os.Exit(127)
	}

	// boolean flags default to false
	result.ExecArgs.Rollback = !result.ExecArgs.Rollback

	return result
}
