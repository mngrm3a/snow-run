package main

import (
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"

	"github.com/antchfx/htmlquery"
	"github.com/pkg/errors"
	"golang.org/x/net/html"
	"golang.org/x/net/publicsuffix"
)

type token struct {
	name  string
	value string
}

type execOpts struct {
	Scope    string `long:"scope" default:"global" description:"scope to run the script in"`
	Rollback bool   `long:"no-rollback" description:"don't record rollback information"`
	Sandbox  bool   `long:"sandbox" description:"execute the script in a sandboxed environment"`
	Quota    bool   `long:"quota" description:"cancel the script after 4 hours"`
}

type execResult struct {
	historyURL string
	output     string
}

type loginDataGeneratorFunc func(string, string) url.Values
type execDataGeneratorFunc func(string, execOpts) url.Values
type abstractScraperFunc func(*html.Node) (interface{}, error)
type tokenScraperFunc func(*html.Node) (token, error)
type stringScraperFunc func(*html.Node) (string, error)

type client struct {
	client            http.Client
	instanceURL       string
	generateLoginData loginDataGeneratorFunc
	generateExecData  execDataGeneratorFunc
	scrapeFormToken   tokenScraperFunc
	scrapeHistoryURL  stringScraperFunc
	scrapeOutput      stringScraperFunc
}

func newKingstonClient(instanceURL string) (client, error) {
	return newClient(
		instanceURL,
		basicLoginDataGenerator,
		basicExecDataGenerator,
		mkGenericTokenScraper("sysparm_ck"),
		noOpStringScraper,
		mkGenericBlockScraper("/html/body/pre"))
}

func newLondonClient(instanceURL string) (client, error) {
	return newClient(
		instanceURL,
		basicLoginDataGenerator,
		func(script string, config execOpts) url.Values {
			result := basicExecDataGenerator(script, config)
			addCheckboxFormValue(&result, "rollback", config.Rollback)
			return result
		},
		mkGenericTokenScraper("sysparm_ck"),
		mkGenericHistoryURLScraper(instanceURL, "/html/body/a"),
		mkGenericBlockScraper("/html/body/pre"))
}

func newClient(instanceURL string, loginDataGenerator loginDataGeneratorFunc, execDataGenerator execDataGeneratorFunc, tokenScraper tokenScraperFunc, historyURLScraper stringScraperFunc, outputScraper stringScraperFunc) (result client, err error) {
	jar, err := cookiejar.New(
		&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return result, errors.Wrap(err, "couldn't initialize the cookie jar")
	}

	httpClient := http.Client{
		Timeout: time.Second * 10,
		Jar:     jar,
	}

	return client{
		client:            httpClient,
		instanceURL:       instanceURL,
		generateLoginData: loginDataGenerator,
		generateExecData:  execDataGenerator,
		scrapeFormToken:   tokenScraper,
		scrapeHistoryURL:  historyURLScraper,
		scrapeOutput:      outputScraper,
	}, nil
}

func (c *client) login(username, password string) error {
	response, err := c.postForm(
		c.instanceURL+"/login.do",
		c.generateLoginData(username, password))
	if err == nil {
		response.Body.Close()
	}

	return err
}

func (c *client) exec(script string, config execOpts) (result execResult, err error) {
	response, err := c.postForm(
		c.instanceURL+"/sys.scripts.do",
		c.generateExecData(script, config))
	if err != nil {
		return result, err
	}
	defer response.Body.Close()

	rawResult, err := scrape(
		response.Body,
		func(node *html.Node) (_ interface{}, err error) {
			var result execResult

			result.historyURL, err = c.scrapeHistoryURL(node)
			if err != nil {
				return result, err
			}

			result.output, err = c.scrapeOutput(node)

			return result, err
		})
	if err != nil {
		return result, err
	}
	result, ok := rawResult.(execResult)
	if !ok {
		errors.New("type assertion failed")
	}

	return result, err
}

func (c *client) postForm(URL string, data url.Values) (*http.Response, error) {
	response, err := responseChecker(true, false)(c.client.Get(URL))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	rawToken, err := scrape(
		response.Body,
		func(node *html.Node) (interface{}, error) {
			return c.scrapeFormToken(node)
		})
	if err != nil {
		return nil, err
	}
	token, ok := rawToken.(token)
	if !ok {
		return nil, errors.New("type assertion failed")
	}

	data.Add(token.name, token.value)

	return responseChecker(true, true)(c.client.PostForm(URL, data))
}

func responseChecker(checkStatus, checkSession bool) func(*http.Response, error) (*http.Response, error) {
	return func(response *http.Response, err error) (*http.Response, error) {
		if err != nil {
			return nil, errors.New("didn't receive a response")
		}

		if checkStatus && response.StatusCode != 200 {
			return nil, errors.Errorf(
				"expected http status 200, got %d",
				response.StatusCode)
		}

		if checkSession && !hasHeaderValue(response.Header, "X-Is-Logged-In", "true") {
			return nil, errors.New("not an active session")
		}

		return response, err
	}
}

func scrape(body io.Reader, scraper abstractScraperFunc) (interface{}, error) {
	document, err := htmlquery.Parse(body)
	if err != nil {
		return "", errors.New("couldn't parse response body")
	}

	return scraper(document)
}
