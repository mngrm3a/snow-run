package main

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/antchfx/htmlquery"
	"github.com/pkg/errors"
	"golang.org/x/net/html"
)

func basicLoginDataGenerator(username, password string) url.Values {
	result := url.Values{}

	result.Add("sys_action", "sysverb_login")
	result.Add("user_name", username)
	result.Add("user_password", password)

	return result
}

func basicExecDataGenerator(script string, args execOpts) url.Values {
	result := url.Values{}

	// runscript value is necessary, otherwise the response body will be empty
	result.Add("runscript", "")
	result.Add("script", script)
	result.Add("sys_scope", args.Scope)
	addCheckboxFormValue(&result, "sandbox", args.Sandbox)
	addCheckboxFormValue(&result, "quota_managed_transaction", args.Quota)

	return result
}

func mkGenericTokenScraper(name string) func(*html.Node) (token, error) {
	return func(document *html.Node) (result token, err error) {
		result.name = name
		result.value, err = getAttributeValue(
			document,
			fmt.Sprintf("//*[@name='%s']", name),
			"value")

		return result, err
	}
}

func noOpStringScraper(*html.Node) (string, error) {
	return "", nil
}

func mkGenericURLScraper(xpathExpr string) func(*html.Node) (string, error) {
	return func(document *html.Node) (string, error) {
		return getAttributeValue(document, xpathExpr, "href")
	}
}

func mkGenericBlockScraper(xpathExpr string) func(*html.Node) (string, error) {
	return func(document *html.Node) (string, error) {
		node, err := getNode(document, xpathExpr)
		if err != nil {
			return "", err
		}

		builder := strings.Builder{}
		for s := node.FirstChild; s != nil; s = s.NextSibling {
			if s.Type == html.TextNode {
				builder.WriteString(s.Data)
				builder.WriteByte(10)
			}
		}

		return builder.String(), nil
	}
}

func mkGenericHistoryURLScraper(instanceURL, xpathExpr string) func(*html.Node) (string, error) {
	return func(document *html.Node) (string, error) {
		url, err := mkGenericURLScraper(xpathExpr)(document)
		if err == nil {
			url = instanceURL + "/" + url
		}

		return url, err
	}
}

func getAttributeValue(document *html.Node, xpathExpr, attrName string) (string, error) {
	node, err := getNode(document, xpathExpr)
	if err != nil {
		return "", err
	}

	return getAttribute(node, attrName)
}

func getNode(rootNode *html.Node, xpathExpr string) (*html.Node, error) {
	node := htmlquery.FindOne(rootNode, xpathExpr)
	if node == nil {
		return nil, errors.Errorf("couldn't find node '%s'", xpathExpr)
	}

	return node, nil
}

func getAttribute(node *html.Node, key string) (result string, err error) {
	for name := range node.Attr {
		if node.Attr[name].Key == key {
			result = node.Attr[name].Val
			// if node.Attr[name].Val == "" {
			// 	return result, errors.Errorf(
			// 		"attribute '%s' in node '%s' is empty",
			// 		key,
			// 		node.Data)
			// }
			return result, err
		}
	}

	return result, errors.Errorf(
		"couldn't find attribute '%s' in node '%s'",
		key,
		node.Data)
}

func hasHeaderValue(headers http.Header, name, value string) bool {
	for headerName := range headers {
		if headerName == name {
			for headerValue := range headers[headerName] {
				if headers[headerName][headerValue] == value {
					return true
				}
			}
		}
	}

	return false
}

func addCheckboxFormValue(data *url.Values, name string, value bool) {
	if value {
		data.Add(name, "on")
	}
}
