# snow-run

Run local scripts as background script on a ServiceNow instance.

## Command line arguments

```txt
Usage:
  snow-run [OPTIONS] [FILE]

Application Options:
  -c, --config=      path of the config file (default: ./instance.json)
  -v, --verbose      be more verbose

Execute script Options:
      --scope=       scope to run the script in (default: global)
      --no-rollback  don't record rollback information
      --sandbox      execute the script in a sandboxed environment
      --quota        cancel the script after 4 hours

Help Options:
  -h, --help         Show this help message
```

## Config file

Configuration files are instance specifc and must provide version and login informations.

```json
{
    "Version": "kingston",
    "Instance": "devXXXX",
    "Username": "admin",
    "Password": "xxxxx-xxxxx-xxxxx"
}
```

## Examples

* Run file ```script.js```, located in the current directory:

  ```shell
  snow-run script.js
  ````

* Specify an alternate configuration file:

  ```shell
  snow-run --config /User/me/config.json script.js
  ````
