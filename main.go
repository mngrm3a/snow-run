package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/pkg/errors"
)

func main() {
	options := parseOptions()
	handleError := func(err error) {
		if err == nil {
			return
		}

		if options.Verbose {
			fmt.Fprintf(os.Stderr, "%+v", err)
		} else {
			fmt.Fprintf(os.Stderr, "%v", err)
		}

		os.Exit(1)
	}

	config, err := readConfigFile(options.Config)
	handleError(err)

	script, err := ioutil.ReadFile(options.Args.Script)
	handleError(errors.Wrap(err, "couldn't read script file"))

	client, err := newClientFromVersion(config.Instance, config.Version)
	handleError(err)

	err = client.login(config.Username, config.Password)
	handleError(err)

	result, err := client.exec(string(script), options.ExecArgs)
	handleError(err)

	if result.historyURL != "" {
		fmt.Println(result.historyURL)
	}
	fmt.Println(result.output)
}

func newClientFromVersion(instanceName, version string) (client, error) {
	instanceURL := fmt.Sprintf("https://%s.service-now.com", instanceName)

	switch version {
	case "kingston":
		return newKingstonClient(instanceURL)
	case "london":
		return newLondonClient(instanceURL)
	default:
		return client{}, errors.Errorf("version '%s' is not supported", version)
	}
}
